export const USER_TITLES = {
  VOLUNTEER: 'volunteer',
  MEMBER: 'member',
  ACTIVE_MEMBER: 'active_member',
  BC: 'bc',
  BCE: 'bce',
  ALUMNUS: 'alumnus'
}
