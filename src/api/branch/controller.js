import {success, notFound, adminWithPermOrSuper} from '../../services/response/'
import { Branch } from '.'

export const create = ({ bodymen: { body }, user }, res, next) =>
  Branch.create(body)
    .then(adminWithPermOrSuper(res, user, 'branches'))
    .then((branch) => branch.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Branch.find(query, select, cursor)
    .then((branches) => branches.map((branch) => branch.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Branch.findById(params.id)
    .then(notFound(res))
    .then((branch) => branch ? branch.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ body, params, user }, res, next) =>
  Branch.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'branches'))
    .then((branch) => {
      if (branch) {
        let newBody = {
          name: body.name ? body.name : branch.name,
          description: body.description ? body.description : branch.description,
          coordinatorId: body.coordinatorId ? body.coordinatorId : branch.coordinatorId
        }
        return Object.assign(branch, newBody).save()
      }
      return false
    })
    .then((branch) => branch ? branch.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params, user }, res, next) =>
  Branch.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'branches'))
    .then((branch) => branch ? branch.remove() : null)
    .then(success(res, 204))
    .catch(next)
