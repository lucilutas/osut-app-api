import mongoose, { Schema } from 'mongoose'

const branchSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  coordinatorId: {
    type: Schema.ObjectId,
    ref: 'User',
    default: null
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

branchSchema.methods = {
  view () {
    return {
      // simple view
      id: this.id,
      name: this.name,
      description: this.description,
      coordinatorId: this.coordinatorId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }
  }
}

const model = mongoose.model('Branch', branchSchema)

export const schema = model.schema
export default model
