import { Branch } from '.'

let branch

beforeEach(async () => {
  branch = await Branch.create({ name: 'test', description: 'test', coordinatorId: null })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = branch.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(branch.id)
    expect(view.name).toBe(branch.name)
    expect(view.description).toBe(branch.description)
    expect(view.coordinatorId).toBe(branch.coordinatorId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
