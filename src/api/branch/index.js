import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Branch, { schema } from './model'

const router = new Router()
const { name, description, coordinatorId } = schema.tree

/**
 * @api {post} /branches Create branch
 * @apiName CreateBranch
 * @apiGroup Branch
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Branch's name.
 * @apiParam description Branch's description.
 * @apiParam coordinatorId Branch's coordinatorId.
 * @apiSuccess {Object} branch Branch's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Branch not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin'] }),
  body({ name, description, coordinatorId }),
  create)

/**
 * @api {get} /branches Retrieve branches
 * @apiName RetrieveBranches
 * @apiGroup Branch
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} branches List of branches.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /branches/:id Retrieve branch
 * @apiName RetrieveBranch
 * @apiGroup Branch
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} branch Branch's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Branch not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /branches/:id Update branch
 * @apiName UpdateBranch
 * @apiGroup Branch
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam name Branch's name.
 * @apiParam description Branch's description.
 * @apiParam coordinatorId Branch's coordinatorId.
 * @apiSuccess {Object} branch Branch's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Branch not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin'] }),
  update)

/**
 * @api {delete} /branches/:id Delete branch
 * @apiName DeleteBranch
 * @apiGroup Branch
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Branch not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

export default router
