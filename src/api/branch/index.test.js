import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Branch } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, branch, user, admin2Session

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin', permissions: ['branches'] })
  const admin2 = await User.create({ email: 'cs@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  admin2Session = signSync(admin2.id)
  branch = await Branch.create({ name: 'test', description: 'test', coordinatorId: user.id })
})

test('POST /branches 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession, name: 'test', description: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.coordinatorId).toEqual(null)
})

test('POST /branches 401 (admin 2)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: admin2Session, name: 'test', description: 'test' })
  expect(status).toBe(401)
})

test('POST /branches 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /branches 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /branches 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /branches 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /branches/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${branch.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(branch.id)
})

test('GET /branches/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${branch.id}`)
  expect(status).toBe(401)
})

test('GET /branches/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /branches/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${branch.id}`)
    .send({ access_token: adminSession, name: 'test', description: 'testw' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(branch.id)
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('testw')
  expect(body.coordinatorId).toEqual(user.id)
})

test('PUT /branches/:id 401 (admin 2)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${branch.id}`)
    .send({ access_token: admin2Session })
  expect(status).toBe(401)
})

test('PUT /branches/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${branch.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /branches/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${branch.id}`)
  expect(status).toBe(401)
})

test('PUT /branches/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: adminSession, name: 'test', description: 'test' })
  expect(status).toBe(404)
})

test('DELETE /branches/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${branch.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /branches/:id 401 (admin 2)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${branch.id}`)
    .query({ access_token: admin2Session })
  expect(status).toBe(401)
})

test('DELETE /branches/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${branch.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /branches/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${branch.id}`)
  expect(status).toBe(401)
})

test('DELETE /branches/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
