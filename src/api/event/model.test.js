import { Event } from '.'

let event

beforeEach(async () => {
  event = await Event.create({ name: 'test', description: 'test', eventDate: new Date() })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = event.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(event.id)
    expect(view.name).toBe(event.name)
    expect(view.description).toBe(event.description)
    expect(view.eventDate).toBe(event.eventDate)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
