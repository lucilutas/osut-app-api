import {success, notFound, adminWithPermOrSuper} from '../../services/response/'
import { Event } from '.'

export const create = ({ bodymen: { body }, user }, res, next) =>
  Event.create(body)
    .then(adminWithPermOrSuper(res, user, 'events'))
    .then((event) => event.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Event.find(query, select, cursor)
    .then((events) => events.map((event) => event.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Event.findById(params.id)
    .then(notFound(res))
    .then((event) => event ? event.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ body, params, user }, res, next) =>
  Event.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'events'))
    .then((event) => {
      if (event) {
        let newBody = {
          name: body.name ? body.name : event.name,
          description: body.description ? body.description : event.description,
          eventDate: body.eventDate ? body.eventDate : event.eventDate
        }
        return Object.assign(event, newBody).save()
      }
      return null
    })
    .then((event) => event ? event.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params, user }, res, next) =>
  Event.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'events'))
    .then((event) => event ? event.remove() : null)
    .then(success(res, 204))
    .catch(next)
