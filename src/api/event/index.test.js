import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Event } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, event, admin2Session

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin', permissions: ['events'] })
  const admin2 = await User.create({ email: 'cd@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  admin2Session = signSync(admin2.id)
  event = await Event.create({name: 'test', description: 'test', eventDate: new Date('2019-08-20T21:51:52.151+00:00')})
})

test('POST /events 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession, name: 'test', description: 'test', eventDate: new Date('2019-08-20T21:51:52.151+00:00') })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.eventDate).toEqual('2019-08-20T21:51:52.151Z')
})

test('POST /events 401 (admin 2)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: admin2Session, name: 'test', description: 'test', eventDate: new Date('2019-08-20T21:51:52.151+00:00') })
  expect(status).toBe(401)
})

test('POST /events 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /events 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /events 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /events 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /events/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${event.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(event.id)
})

test('GET /events/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${event.id}`)
  expect(status).toBe(401)
})

test('GET /events/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /events/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${event.id}`)
    .send({ access_token: adminSession, name: 'test', description: 'testw' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(event.id)
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('testw')
  expect(body.eventDate).toEqual('2019-08-20T21:51:52.151Z')
})

test('PUT /events/:id 401 (admin 2)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${event.id}`)
    .send({ access_token: admin2Session })
  expect(status).toBe(401)
})

test('PUT /events/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${event.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /events/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${event.id}`)
  expect(status).toBe(401)
})

test('PUT /events/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: adminSession, name: 'test', description: 'test', eventDate: 'test' })
  expect(status).toBe(404)
})

test('DELETE /events/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${event.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /events/:id 401 (admin 2)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${event.id}`)
    .query({ access_token: admin2Session })
  expect(status).toBe(401)
})

test('DELETE /events/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${event.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /events/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${event.id}`)
  expect(status).toBe(401)
})

test('DELETE /events/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
