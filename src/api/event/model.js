import mongoose, { Schema } from 'mongoose'

const eventSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  eventDate: {
    type: Date
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

eventSchema.methods = {
  view () {
    return {
      // simple view
      id: this.id,
      name: this.name,
      description: this.description,
      eventDate: this.eventDate,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }
  }
}

const model = mongoose.model('Event', eventSchema)

export const schema = model.schema
export default model
