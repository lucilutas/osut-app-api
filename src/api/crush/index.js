import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Crush, { schema } from './model'

const router = new Router()
const { longText } = schema.tree

/**
 * @api {post} /crushes Create crush
 * @apiName CreateCrush
 * @apiGroup Crush
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam number Crush's number.
 * @apiParam shortText Crush's shortText.
 * @apiParam longText Crush's longText.
 * @apiParam approved Crush's approved.
 * @apiSuccess {Object} crush Crush's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Crush not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ longText }),
  create)

/**
 * @api {get} /crushes Retrieve crushes
 * @apiName RetrieveCrushes
 * @apiGroup Crush
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} crushes List of crushes.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /crushes/:id Retrieve crush
 * @apiName RetrieveCrush
 * @apiGroup Crush
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} crush Crush's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Crush not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /crushes/:id Update crush
 * @apiName UpdateCrush
 * @apiGroup Crush
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam number Crush's number.
 * @apiParam shortText Crush's shortText.
 * @apiParam longText Crush's longText.
 * @apiParam approved Crush's approved.
 * @apiSuccess {Object} crush Crush's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Crush not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin'] }),
  update)

/**
 * @api {delete} /crushes/:id Delete crush
 * @apiName DeleteCrush
 * @apiGroup Crush
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Crush not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

export default router
