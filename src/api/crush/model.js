import mongoose, { Schema } from 'mongoose'

const crushSchema = new Schema({
  number: {
    type: Number,
    default: null
  },
  shortText: {
    type: String
  },
  longText: {
    type: String,
    required: true
  },
  approved: {
    type: Boolean,
    default: false
  },
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

crushSchema.methods = {
  view (full) {
    return {
      // simple view
      id: this.id,
      createdBy: this.createdBy.view(full),
      number: this.number,
      shortText: this.shortText,
      longText: this.longText,
      approved: this.approved,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }
  }
}

const model = mongoose.model('Crush', crushSchema)

export const schema = model.schema
export default model
