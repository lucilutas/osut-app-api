import {success, notFound, adminWithPermOrSuper} from '../../services/response/'
import { Crush } from '.'
import _ from 'lodash'

export const create = ({ user, bodymen: { body } }, res, next) => {
  body.shortText = ''
  let longTextSplit = body.longText.split(' ')
  for (let i = 0; i < 8; i++) {
    if (i <= longTextSplit.length) {
      body.shortText += longTextSplit[i] + ' '
    }
  }
  if (longTextSplit.length > 8) {
    body.shortText += '...'
  }
  return Crush.create({...body, createdBy: user})
    .then((crush) => crush.view(true))
    .then(success(res, 201))
    .catch(next)
}

export const index = ({ querymen: { query, select, cursor }, user }, res, next) => {
  if (!user.permissions.includes('crushes') && (user.role !== 'admin' || user.role !== 'superAdmin')) {
    query.approved = true
  }
  return Crush.find(query, select, cursor)
    .then((crushes) => crushes.map((crush) => crush.view()))
    .then(success(res))
    .catch(next)
}

export const show = ({ params }, res, next) =>
  Crush.findById(params.id)
    .then(notFound(res))
    .then((crush) => crush ? crush.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ body, params, user }, res, next) =>
  Crush.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'crushes'))
    .then(async (crush) => {
      if (crush) {
        let newBody = {
          longText: body.longText ? body.longText : crush.longText,
          approved: _.isNull(body.approved) || _.isUndefined(body.approved) ? crush.approved : body.approved
        }
        newBody.shortText = ''
        let longTextSplit = newBody.longText.split(' ')
        for (let i = 0; i < 8; i++) {
          if (i <= longTextSplit.length) {
            newBody.shortText += longTextSplit[i] + ' '
          }
        }
        if (longTextSplit.length > 8) {
          newBody.shortText += '...'
        }
        if (body.approved === true && crush.approved === false) {
          newBody.number = await Crush.find({approved: true}).then((crushes) => { return crushes.length + 1 })
        }
        return Object.assign(crush, newBody).save()
      }
      return null
    })
    .then((crush) => crush ? crush.view() : null)
    .then(success(res))
    .catch(e => {
      console.log(e)
    })

export const destroy = ({ params, user }, res, next) =>
  Crush.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'crushes'))
    .then((crush) => crush ? crush.remove() : null)
    .then(success(res, 204))
    .catch(next)
