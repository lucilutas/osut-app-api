import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Crush } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, crush, admin2Session

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin', permissions: ['crushes'] })
  const admin2 = await User.create({ email: 'cs@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  admin2Session = signSync(admin2.id)
  crush = await Crush.create({ createdBy: user, longText: 'Duis luctus, est at tincidunt interdum, erat ex elementum quam, sit amet molestie neque velit eu ipsum. Mauris a tempor sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra' })
  await Crush.create({ createdBy: user, number: 1, longText: 'Duis luctus, est at tincidunt interdum, erat ex elementum quam, sit amet molestie neque velit eu ipsum. Mauris a tempor sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra', approved: true })
})

test('POST /crushes 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, shortText: 'test', longText: 'Duis luctus, est at tincidunt interdum, erat ex elementum quam, sit amet molestie neque velit eu ipsum. Mauris a tempor sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.number).toEqual(null)
  expect(body.shortText).toEqual('Duis luctus, est at tincidunt interdum, erat ex ...')
  expect(body.longText).toEqual('Duis luctus, est at tincidunt interdum, erat ex elementum quam, sit amet molestie neque velit eu ipsum. Mauris a tempor sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra')
  expect(body.approved).toEqual(false)
  expect(typeof body.createdBy).toEqual('object')
})

test('POST /crushes 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /crushes 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
  expect(typeof body[0].createdBy).toEqual('object')
  expect(body.length).toEqual(2)
})

test('GET /crushes 200 (admin 2)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: admin2Session })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
  expect(typeof body[0].createdBy).toEqual('object')
  expect(body.length).toEqual(1)
})

test('GET /crushes 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
  expect(typeof body[0].createdBy).toEqual('object')
  expect(body.length).toEqual(1)
})

test('GET /crushes 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /crushes/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${crush.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(crush.id)
  expect(typeof body.createdBy).toEqual('object')
})

test('GET /crushes/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${crush.id}`)
  expect(status).toBe(401)
})

test('GET /crushes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /crushes/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${crush.id}`)
    .send({ access_token: adminSession, approved: true })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(crush.id)
  expect(body.number).toEqual(2)
  expect(body.shortText).toEqual('Duis luctus, est at tincidunt interdum, erat ex ...')
  expect(body.longText).toEqual('Duis luctus, est at tincidunt interdum, erat ex elementum quam, sit amet molestie neque velit eu ipsum. Mauris a tempor sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra')
  expect(body.approved).toEqual(true)
})

test('PUT /crushes/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${crush.id}`)
    .send({ access_token: adminSession, longText: 'Aenean porttitor tortor ut ipsum consequat rhoncus. Cras auctor diam non magna cursus' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(crush.id)
  expect(body.number).toEqual(null)
  expect(body.shortText).toEqual('Aenean porttitor tortor ut ipsum consequat rhoncus. Cras ...')
  expect(body.longText).toEqual('Aenean porttitor tortor ut ipsum consequat rhoncus. Cras auctor diam non magna cursus')
  expect(body.approved).toEqual(false)
})

test('PUT /crushes/:id 401 (admin 2)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${crush.id}`)
    .send({ access_token: admin2Session })
  expect(status).toBe(401)
})

test('PUT /crushes/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${crush.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /crushes/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${crush.id}`)
  expect(status).toBe(401)
})

test('PUT /crushes/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: adminSession, approved: true })
  expect(status).toBe(404)
})

test('DELETE /crushes/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${crush.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /crushes/:id 401 (admin 2)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${crush.id}`)
    .query({ access_token: admin2Session })
  expect(status).toBe(401)
})

test('DELETE /crushes/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${crush.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /crushes/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${crush.id}`)
  expect(status).toBe(401)
})

test('DELETE /crushes/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
