import { Crush } from '.'
import { User } from '../user'

let user, crush

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  crush = await Crush.create({ createdBy: user, number: 1, shortText: 'test', longText: 'test', approved: false })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = crush.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(crush.id)
    expect(typeof view.createdBy).toBe('object')
    expect(view.createdBy.id).toBe(user.id)
    expect(view.number).toBe(crush.number)
    expect(view.shortText).toBe(crush.shortText)
    expect(view.longText).toBe(crush.longText)
    expect(view.approved).toBe(crush.approved)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
