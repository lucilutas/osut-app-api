import { success, notFound, adminWithPermOrSuper } from '../../services/response/'
import { User } from '.'
import { sign } from '../../services/jwt'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  User.find(query, select, cursor)
    .then((users) => users.map((user) => user.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const showMe = ({ user }, res) =>
  res.json(user.view(true))

export const create = ({ bodymen: { body } }, res, next) =>
  User.create(body)
    .then(user => {
      sign(user.id)
        .then((token) => ({ token, user: user.view() }))
        .then(success(res, 201))
    })
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          success: false,
          message: 'EMAIL_ALREADY_REGISTERED'
        })
      } else {
        next(err)
      }
    })

export const update = ({ body, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isAdminWithPerm = user.role === 'admin' && user.permissions.include('users')
      const isSuperAdmin = user.role === 'superAdmin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdminWithPerm && isSuperAdmin) {
        res.status(401).json({
          success: false,
          message: 'UNAUTHORIZED'
        })
        return null
      }
      return result
    })
    .then((user) => {
      if (user) {
        let newBody = {
          firstName: body.firstName ? body.firstName : user.firstName,
          lastName: body.lastName ? body.lastName : user.lastName,
          picture: body.picture ? body.picture : user.picture,
          email: body.email ? body.email : user.email,
          title: body.title ? body.title : user.title
        }
        return Object.assign(user, newBody).save()
      }
      return null
    })
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const updatePassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          success: false,
          message: 'UNAUTHORIZED'
        })
        return null
      }
      return result
    })
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params, user }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then(adminWithPermOrSuper(res, user, 'users'))
    .then((user) => user ? user.remove() : null)
    .then(success(res, 204))
    .catch(next)
