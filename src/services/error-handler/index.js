exports.errorHandler = errorHandler

function errorHandler () {
  return function (err, req, res, next) {
    if (req.bodymen && req.bodymen.error) {
      if (req.bodymen.error.name === 'minlength') {
        res.status(400).json({
          success: false,
          message: req.bodymen.error.param.toUpperCase() + '_TOO_SHORT'
        })
      }
      if (req.bodymen.error.name === 'required') {
        res.status(400).json({
          success: false,
          message: req.bodymen.error.param.toUpperCase() + '_IS_REQUIRED'
        })
      }
      if (req.bodymen.error.name === 'match' || req.bodymen.error.name === 'enum') {
        res.status(422).json({
          success: false,
          message: req.bodymen.error.param.toUpperCase() + '_IS_INVALID'
        })
      }
    } else if (err) {
      if (err.name === 'CastError') {
        res.status(422).json({
          success: false,
          message: err.path.replace(/[\W_]+/g, '').toUpperCase() + '_IS_INVALID'
        })
      }
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          success: false,
          message: 'EMAIL_ALREADY_REGISTERED'
        })
      }
      if (err.name === 'ValidationError') {
        res.status(422).json({
          success: false,
          message: err.message.split('`')[1].toUpperCase() + '_IS_INVALID'
        })
      }
    } else {
      next(err)
    }
  }
}

exports.default = { errorHandler }
